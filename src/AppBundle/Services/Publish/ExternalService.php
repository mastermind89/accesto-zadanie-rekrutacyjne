<?php

namespace AppBundle\Services\Publish;

abstract class ExternalService implements IExternalService
{
    protected $message;

    public function __construct()
    {
        $this->message = 'Post published ';
    }

    public function publish()
    {
        return $this->message;
    }
}