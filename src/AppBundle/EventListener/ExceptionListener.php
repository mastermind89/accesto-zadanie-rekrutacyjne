<?php

namespace AppBundle\EventListener;

use AppBundle\Exception\InvalidDataProvidedException;
use AppBundle\Exception\TargetNotExistsException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($event->getException() instanceof ResourceNotFoundException) {
            $event->setResponse(new Response('Error: blog post not found'));
        }

        if ($event->getException() instanceof InvalidDataProvidedException) {
            $event->setResponse(new Response('Error: invalid data provided'));
        }

        if ($event->getException() instanceof TargetNotExistsException) {
            $event->setResponse(new Response('Error: target service does not exist'));
        }
    }
}
