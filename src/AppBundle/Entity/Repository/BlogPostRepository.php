<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\BlogPost;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class BlogPostRepository.
 */
class BlogPostRepository extends EntityRepository
{
    /**
     * @param BlogPost $post
     * @return BlogPost
     * @throws OptimisticLockException
     */
    public function saveAndRefresh(BlogPost $post)
    {
        $this->_em->persist($post);
        $this->_em->flush();
        $this->_em->refresh($post);

        return $post;
    }
}
