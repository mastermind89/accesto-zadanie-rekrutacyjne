<?php

namespace AppBundle\Services\Publish;

interface IExternalService
{
    public function publish();
}