<?php

namespace AppBundle\Exception;

/**
 * Class InvalidDataProvidedException.
 */
class InvalidDataProvidedException extends \Exception
{
}
