<?php

namespace AppBundle\Services\Publish\Compiler;

use AppBundle\Services\Publish\PublishersService;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class PublisherPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(PublishersService::class)) {
            return;
        }

        $definition = $container->findDefinition(PublishersService::class);

        $taggedServices = $container->findTaggedServiceIds('app.publish');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addPublisher', [
                    new Reference($id),
                    $attributes['alias']
                ]);
            }
        }
    }
}