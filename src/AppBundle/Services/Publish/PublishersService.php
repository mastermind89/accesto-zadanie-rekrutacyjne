<?php

namespace AppBundle\Services\Publish;

use AppBundle\Exception\TargetNotExistsException;

class PublishersService
{
    private $publishers;

    public function __construct()
    {
        $this->publishers = array();
    }

    public function addPublisher($publisher, $alias)
    {
        $this->publishers[$alias] = $publisher;
    }

    public function getPublisher($alias)
    {
        if (array_key_exists($alias, $this->publishers)) {
            return $this->publishers[$alias];
        }

        throw new TargetNotExistsException();
    }
}