<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\BlogPost;
use AppBundle\Entity\Repository\BlogPostRepository;
use AppBundle\Exception\InvalidDataProvidedException;
use AppBundle\Exception\TargetNotExistsException;
use AppBundle\Form\BlogPostType;
use AppBundle\Services\Publish\PublishersService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Class BlogPostController.
 */
class BlogPostController extends FOSRestController
{
    /** @var EntityManagerInterface $entityManager */
    private $entityManager;

    /** @var BlogPostRepository $blogPostRepository */
    private $blogPostRepository;

    /** @var PublishersService $publishersService */
    private $publishersService;

    public function __construct(
        EntityManagerInterface $entityManager,
        BlogPostRepository $blogPostRepository,
        PublishersService $publishersService
    ) {
        $this->entityManager = $entityManager;
        $this->blogPostRepository = $blogPostRepository;
        $this->publishersService = $publishersService;
    }

    /**
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Return complete list of blog posts"
     * )
     *
     * @Route(name="api.blog_post.list", path="/blog-post")
     * @Method("GET")
     *
     * @return View
     */
    public function listPostsAction()
    {
        return $this->view($this->blogPostRepository->findAll());
    }

    /**
     * @Method("POST")
     * @Route(name="api.blog_post.create", path="/blog-post")
     * @param Request $request
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Create new blog post",
     *     parameters={
     *          {"name"="title", "dataType"="string", "required"=true, "description"="title of blog post"},
     *          {"name"="content", "dataType"="string", "required"=true, "description"="content of blog post"},
     *          {"name"="tags", "dataType"="string", "required"=true, "description"="tags of blog post"},
     *      },
     *      output={"class"="AppBundle\Entity\BlogPost"}
     * )
     * @throws InvalidDataProvidedException
     * @throws OptimisticLockException
     * @return View
     */
    public function createPostAction(Request $request)
    {
        $post = new BlogPost();

        $form = $this->createForm(
            BlogPostType::class,
            $post,
            [
                'csrf_protection' => false,
                'method' => 'PATCH',
                'allow_extra_fields' => true
            ]
        );
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            throw new InvalidDataProvidedException();
        }

        $post = $this->blogPostRepository->saveAndRefresh($post);

        return $this->view($post);
    }

    /**
     * @Method("PATCH")
     * @Route(name="api.blog_post.edit", path="/blog-post/{id}")
     * @param Request $request
     * @param int $id
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Edit blog post by id",
     *     parameters={
     *          {"name"="title", "dataType"="string", "required"=false, "description"="title of blog post"},
     *          {"name"="content", "dataType"="string", "required"=false, "description"="content of blog post"},
     *          {"name"="tags", "dataType"="string", "required"=false, "description"="tags of blog post"},
     *      },
     *      output={"class"="AppBundle\Entity\BlogPost"}
     * )
     * @throws InvalidDataProvidedException
     * @throws OptimisticLockException
     * @return View
     */
    public function editPostAction(Request $request, $id)
    {
        /** @var BlogPost|null $post */
        $post = $this->blogPostRepository->find($id);

        if (!$post instanceof BlogPost) {
            throw new ResourceNotFoundException();
        }

        $form = $this->createForm(
            BlogPostType::class,
            $post,
            [
                'csrf_protection' => false,
                'method' => 'PATCH',
                'allow_extra_fields' => true
            ]
        );
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            throw new InvalidDataProvidedException();
        }

        $post = $this->blogPostRepository->saveAndRefresh($post);

        return $this->view($post);
    }

    /**
     * @Method("POST")
     * @Route(name="api.blog_post.publish", path="/blog-post/{post}/{target}")
     * @param BlogPost $post
     * @param $target
     * @return View
     * @throws TargetNotExistsException
     * @ApiDoc(
     *     section="Blog Post",
     *     description="Publish post to specified target"
     * )
     */
    public function publishPostAction(BlogPost $post, $target)
    {
        /** @var BlogPost|null $post */
        $post = $this->blogPostRepository->find($post);

        if (!$post instanceof BlogPost) {
            throw new ResourceNotFoundException();
        }

        $publisherService = $this->publishersService->getPublisher($target);

        return $this->view($publisherService->publish());
    }
}
