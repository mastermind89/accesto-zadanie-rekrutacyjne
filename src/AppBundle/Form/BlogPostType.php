<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BlogPostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'required' => true
            ))
            ->add('content', TextType::class, array(
                'required' => true
            ))
            ->add('tags', TextType::class, array(
                'required' => true
            ))
            ->get('tags')
                ->addModelTransformer(new CallbackTransformer(
                    function ($tagsAsArray) {
                        return implode(';', $tagsAsArray);
                    },
                    function ($tagsAsString) {
                        return explode(';', $tagsAsString);
                    }
                ))
            ;
        ;
    }
}
