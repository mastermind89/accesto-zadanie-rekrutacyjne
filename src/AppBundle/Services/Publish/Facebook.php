<?php

namespace AppBundle\Services\Publish;

class Facebook extends ExternalService
{
    public function __construct()
    {
        parent::__construct();
        $this->message .= 'to Facebook';
    }

    public function publish()
    {
        return $this->message;
    }
}
