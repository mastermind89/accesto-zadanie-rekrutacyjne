<?php

namespace AppBundle\Services\Publish;

class Twitter extends ExternalService
{
    public function __construct()
    {
        parent::__construct();
        $this->message .= 'to Twitter';
    }

    public function publish()
    {
        return $this->message;
    }
}
